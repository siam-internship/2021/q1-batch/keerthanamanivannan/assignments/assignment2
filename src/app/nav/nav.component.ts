import { Component, OnInit } from '@angular/core';
import  product from '../../assets/product.json';
import { CartServiceService } from '../cartservice.service';


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  
  products: any[]= product;
  searchTerm: any;

 
constructor(private cartservice : CartServiceService) {}

addProductsToCart(product : any) {
  this.cartservice.addProductsToCart(product);
  //window.alert('Your product added to the cart!');
}
   
  
  

   ngOnInit(): void {

  }

}
