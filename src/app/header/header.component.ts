import { Component, OnInit } from '@angular/core';
import { CartServiceService } from '../cartservice.service';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

tax: number=6.25;



cartproducts = this.cartservice.getProducts();


  constructor(private cartservice : CartServiceService) { }
 
 getTotal(){
   let total=0;
    for(var i=0;i<this.cartproducts.length;i++)
    {
      var product=this.cartproducts[i];
      total=total+ (product.price*product.qty);
    }
    return total;
 }
 
 /*getQuantity(){
  let quantity:number=0;
   for(var i=0;i<this.cartproducts.length;i++)
   {
     var qty=this.cartproducts[i];
     quantity=quantity+qty;
   }
   return quantity;
}*/

public downloadPDF():void{

  const dwdPDF=document.getElementById('pdfconvert');
  html2canvas(dwdPDF).then(canvas=>{
    var imgWidth =108;
    var imgHeight = canvas.height * imgWidth / canvas.width;
  
    const contentDataURL = canvas.toDataURL('image/png')
    let pdf=new jsPDF('p','mm','a4');
    var position = 0;
    pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
    
  pdf.output("dataurlnewwindow");
  pdf.save("product-app.pdf");
})
}

  ngOnInit(): void {
    
  }
}
  

