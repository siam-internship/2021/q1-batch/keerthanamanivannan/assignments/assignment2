import { NgModule } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';
import { NavComponent } from './nav/nav.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

const routes: Routes=[
  { path: 'nav', component: NavComponent},
  { path: 'header', component: HeaderComponent},
  { path: 'footer', component: FooterComponent}
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
